import {Component, Inject, OnInit} from '@angular/core';
import { NavbarService } from 'src/navbar.service';
import { Product } from '../Model/Product';
import { ProductsService } from '../products/products.service';
import {MSAL_GUARD_CONFIG, MsalBroadcastService, MsalGuardConfiguration, MsalService} from "@azure/msal-angular";
import {AuthenticationResult, InteractionType, PopupRequest, RedirectRequest} from "@azure/msal-browser";

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})


export class ViewComponent implements OnInit {

  public ItemsArray: Product[] = [];

  constructor(private service : ProductsService,public nav:NavbarService,
  @Inject(MSAL_GUARD_CONFIG) private msalGuardConfig: MsalGuardConfiguration,
  private authService: MsalService,
  private msalBroadcastService: MsalBroadcastService) { }

  ngOnInit(): void {
    this.nav.show();
    this.nav.doSomethingElseUseful();
    this.getDataApi();

  }

  getDataApi(){
    this.service.getData().subscribe((res: any[])=> {
        this.ItemsArray = res;
    });
  }

  logout() {
    if (this.msalGuardConfig.interactionType === InteractionType.Popup) {
      this.authService.logoutPopup({
        postLogoutRedirectUri: "/",
        mainWindowRedirectUri: "/"
      });
    } else {
      this.authService.logoutRedirect({
        postLogoutRedirectUri: "/",
      });
    }
  }


}

