import {  NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ViewComponent } from './view/view.component';
import {MsalGuard} from "@azure/msal-angular";

const routes: Routes = [

  {
    path: 'ProductEdit',
    component: ProductEditComponent,
    canActivate: [
      MsalGuard
    ]
  },
  {
    path: 'CreateProduct',
    component: CreateComponent,
    canActivate: [
      MsalGuard
    ]
  },
  {
    path: 'EditProduct',
    component: EditComponent,
    canActivate: [
      MsalGuard
    ]
  },
  {
    path: 'ViewProduct',
    component: ViewComponent,
    canActivate: [
      MsalGuard
    ]
  },
  {
    path: '**',
    redirectTo: 'ViewProduct'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
